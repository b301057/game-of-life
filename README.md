# Game Of Life

The code is being written as a learning exercise that follows the tutorial by Real Python's on coding the Game of Life [GoL](https://realpython.com/conway-game-of-life-python/) in Python. 
My personal goals are to understand: 
1. How GoL works?
2. The logic used to implement it in Python 
3. Introduction to collections, curses and argparse libraries.
4. Creating a project structure in python. 
